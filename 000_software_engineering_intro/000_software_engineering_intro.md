# Software Engineering and the Software Development Life Cycle

This chapter covers the following topics:

* A definition of software engineering
* The types of software engineering roles that you may encounter in contemporary organizations
* An overview of popular software development models and which one to select based on the project type and requirements

## What is software engineering?

*"Software engineering is defined as the application of a systematic, disciplined, quantifiable approach to the development, operation, and maintenance of software."*

Besides the obviously high-quality code Software Engineering tries to answer the not so obvious product questions:
* What are the business use cases that the software needs to support?
* What components comprise the system and how do they interact with each other?
* Which technologies will be used to implement the various system components?
* How will the software be tested to ensure that its behavior matches the customer's expectations?
* How does load affect the system's performance and what is the plan for scaling the system?

Software Engineer == **special set of skills** that go beyond programming

## Types of software engineering roles
Some engineers work across all of the software development phases, while others focus on one of the many.

### The role of the software engineer (SWE)
* the most common role
* they play a pivotal role 
    * in designing and building new pieces of software, 
    * and also in operating and maintaining existing and legacy systems
* based on experience:
    * junior
    * medior
    * senior
* based on teir main focus:
    * frontend engineers
    * backend engineers
    * full stack engineers

### The role of the software development engineer in test (SDET)
* individuals who, just like their SWE counterparts, take part in software development, but their primary focus lies in software testing and performance
* they build and operate special infrastrucutre to execute various tests, like Continuous Integration (CI)

### The role of the site reliability engineer (SRE)
SREs spend approximately 50% of their time developing software and the other 50% dealing with ops-related aspects such as the following:
* Working on support tickets or responding to alerts
* Being on-call
* Running manual tasks (for example, upgrading systems or running disaster recovery scenarios)

### The role of the release engineer (RE)
Effectively a software engineer who collaborates with all the engineering teams to define and document all the required steps and processes for building and releasing code to production

### The role of the system architect
Is the one person who sees the big picture: what components comprise the system, how each component must be implemented, and how all the components fit and interact with each other.